<?php if (!defined('STDIN')): ?>
  <style>
      <?php include 'style.css'; ?>
  </style>
<?php endif; ?>
<?php

  include 'matrix.php';

  if (defined('STDIN')) {
      try {
          echo "Podaj wymiary macierzy: \n";
          $rows = readline("Liczba rzędów: ");
          $cols = readline("Liczba kolumn: ");
          $matrix = new Matrix($rows, $cols);
          echo $matrix->printMatrix_Console();
          echo $matrix->readSpiralMatrix_Console();
      } catch (TypeError $e) {
          echo "Błąd podczas podawania danych, spróbuj jeszcze raz!";
      } catch (Exception $e) {
          echo $e->getMessage();
      } catch (Error $e) {
          echo "Błąd podczas podawania danych, spróbuj jeszcze raz!";
      }
  } else {
      try {
          echo '<form name="matrix_form" action="index.php" method="post">
              Podaj liczbę wierszy: <input type="text" name="rows" id="rows" required/>
              Podaj liczbę kolumn: <input type="text" name="cols" id="cols" required/>
             <input type="submit" value="Wyślij" />
          </form>';

          if (!empty($_POST['rows']) && !empty($_POST['cols'])) {
              $matrix = new Matrix((int) $_POST['rows'], (int) $_POST['cols']);
              echo $matrix->printMatrix_HTML();
              echo $matrix->readSpiralMatrix_HTML();
          }
      } catch (TypeError $e) {
          echo "Błąd podczas podawania danych, spróbuj jeszcze raz!";
      } catch (Exception $e) {
          echo $e->getMessage();
      } catch (Error $e) {
          echo "Błąd podczas podawania danych, spróbuj jeszcze raz!";
      }
  }

?>
