<?php

class Matrix
{
    private $rows = 3;
    private $cols = 3;
    private $matrix = [];

    public function __construct(int $rows = 3, int $cols = 3)
    {
        $this->rows = $rows;
        $this->cols = $cols;
        $this->checkExceptions();
        $this->matrix = ($this->rows != 3 || $this->cols != 3) ? $this->createMatrix() : $this->defaultMatrix();
    }

    private function createMatrix(): array
    {
        $matrix = [];
        for ($i=0; $i <= $this->rows-1; $i++) {
            for ($j=0; $j <= $this->cols-1; $j++) {
                $matrix[$i][$j] = rand(0, 200);
            }
        }
        return $matrix;
    }

    private function defaultMatrix(): array
    {
        return $matrix = [
          ["A1", "A2", "A3"],
          ["B1", "B2", "B3"],
          ["C1", "C2", "C3"],
        ];
    }

    private function checkAndReturnLastChar(int $counter): string
    {
        return ($counter == $this->cols * $this->rows) ? "<br />" : ", ";
    }

    public function readSpiralMatrix_HTML(): string
    {
        if (empty($this->matrix)) {
            echo "Macierz jest pusta";
        }

        $route = "<br />Wynik: <br />";
        $counter = 0;

        $top = $left = 0;
        $bottom = $this->rows - 1;
        $right  = $this->cols - 1;

        $direction = 1;
        // $direction == 1 - w prawo
        // $direction == 2 - w dół
        // $direction == 3 - w lewo
        // $direction == 4 - w górę

        while ($top <= $bottom && $left <= $right) {
            if ($direction == 1) {
                for ($i = $left; $i <= $right; ++$i) {
                    $route .= $this->matrix[$top][$i] . ($this->checkAndReturnLastChar(++$counter));
                }
                ++$top;
                $direction = 2;
            } elseif ($direction == 2) {
                for ($i = $top; $i <= $bottom; ++$i) {
                    $route .= $this->matrix[$i][$right] . ($this->checkAndReturnLastChar(++$counter));
                }
                --$right;
                $direction = 3;
            } elseif ($direction == 3) {
                for ($i = $right; $i >= $left; --$i) {
                    $route .= $this->matrix[$bottom][$i] . ($this->checkAndReturnLastChar(++$counter));
                }
                --$bottom;
                $direction++;
            } elseif ($direction == 4) {
                for ($i = $bottom; $i >= $top; --$i) {
                    $route .= $this->matrix[$i][$left] . ($this->checkAndReturnLastChar(++$counter));
                }
                ++$left;
                $direction = 1;
            }
        }

        return $route;
    }

    public function printMatrix_HTML(): string
    {
        $table = "<table class='matrix'>";

        for ($i=0; $i < $this->rows; $i++) {
            $table .= "<tr>";
            for ($j=0; $j < $this->cols; $j++) {
                $table .= "<td align='center'> {$this->matrix[$i][$j]} </td>";
            }
            $table .= "</tr>";
        }
        $table .= "</table>";
        return $table;
    }

    public function readSpiralMatrix_Console(): string
    {
        $text = $this->readSpiralMatrix_HTML();
        return str_replace("<br />", "\n", $text);
    }

    public function printMatrix_Console(): string
    {
        $content = "";
        $mask = "| %3s  %3s  %3s  %3s |\n";
        for ($i=0; $i < $this->rows; $i++) {
            $content .= vsprintf($mask, $this->matrix[$i]);
        }
        return $content;
    }

    private function checkExceptions()
    {
        if ($this->rows <= 0 || $this->cols <= 0) {
            throw new Exception("Podana wartość jest nieprawidłowa!");
        }

        if ($this->rows >= 100 && $this->cols >= 100) {
            throw new Exception("Może nie przesadzajmy z rozmiarem? :)");
        }
    }
}
